'use strict';

const app = require('./app');

app.listen(9900, () => {
  console.log(`Example app listening on port 9900!`);
});