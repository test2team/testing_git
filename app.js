'use strict';

const express = require('express'),
  path = require('path'),
  exphbs = require('express-handlebars'),
  handle_bars_helpers = require('./helpers/handlebars_helpers'),
  serveStatic = require('serve-static'),
  bodyParser = require('body-parser'),
  app = express();

app.engine('.hbs', exphbs({
  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: path.join(__dirname, 'views/layouts'),
  helpers: handle_bars_helpers
}));
app.set('view engine', '.hbs');
app.set('index', path.join(__dirname, 'views'));
app.use('/assets', express.static(path.join(__dirname, 'assets')));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.render('index', {
    data: {}
  });
});
app.get('/favicon.ico', function(req, res) {
  res.status(204);
});

module.exports = app;